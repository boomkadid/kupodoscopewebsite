#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function 
from flask import Flask, render_template, Response, request, redirect, url_for, flash, g
import sys,os,cv2,numpy,base64
sys.path.insert(0, 'src/')
import string,random,time, colormap, createPDF, dbOP, foot

app = Flask(__name__)

@app.route('/',methods=['GET','POST'])
def index():
	try:
		error = request.args['error']
	except Exception as e:
		error = ''
	try:
		if request.method == "POST":
			attemped_userId = request.form['userId']
			return redirect(url_for('dashboard',userId=attemped_userId))
	
		return render_template("index.html",error=error)


	except Exception as e:
		return render_template('index.html',error=e)


@app.route('/dashboard')
def dashboard():
	try:
		userId = request.args['userId'] 
	except:
		return redirect(url_for('index',error="Enter your ID."))

	user = dbOP.query_user('select * from User where user_id = ?',
        [request.args['userId']], one=True)
	photo = dbOP.query_photo('select * from Result where user_id = ?',
		[request.args['userId']], one = False)
	if user is None:
	   return redirect(url_for('index',error="Invalid credentials. Try Again."))
	else:
		#create directory for result storage
		command = "mkdir static\\dataset\\"+userId
		os.system(command)
		return render_template('dashboard.html',user=user,photos=photo)

@app.route('/podo/<userId>', methods=['GET','POST'])
def podo(userId=None):
	if request.method == "POST": #all calulation done here
		result_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(40))
		try:
			user = dbOP.query_user('select * from User where user_id = ?',[userId], one=True)
			os.system("mkdir static\\dataset\\"+userId+"\\"+result_name)
			#add form description
			imgstring64 = request.form['imagebase64'][22:]
			if request.form['description']:
				description = request.form['description']
			else:
				description = 'No description'

			filepath = "static/"+'dataset/'+userId+'/'+result_name


			store64Image(imgstring64,filepath+'/1.png')
			print('store64image done', file=sys.stderr)

			(result_left,result_right) = foot.pasImage(filepath)	
			print('splitstoreimage done', file=sys.stderr)
			
			colormap.process(filepath)
			print('mapcolordone & save', file=sys.stderr)

			result =  dict(result_name=result_name,result=str(result_left)+'&'+str(result_right),result_date=time.strftime("%d/%m/%Y"),
				description=description,user_id=userId)

			createPDF.generate_certificate(user,result,len(dbOP.query_photo('select * from Result where user_id = ?',[userId], one = False))+1)			
			createPDF.generate_normal(user,result,len(dbOP.query_photo('select * from Result where user_id = ?',[userId], one = False))+1,1)			
			createPDF.generate_normal(user,result,len(dbOP.query_photo('select * from Result where user_id = ?',[userId], one = False))+1,2)			
			print('create pdf done', file=sys.stderr)
			

			query = "INSERT INTO Result(result_name,result,result_date,description,user_id) VALUES (?,?,?,?,?)"
			args = (result_name,str(result_left)+'&'+str(result_right) ,time.strftime("%d/%m/%Y"),description,userId) 
			dbOP.insert(query,args)
			print('insert result complete!', file=sys.stderr)
			return redirect(url_for('dashboard',userId=userId))

		except Exception as e:
			print(e, file=sys.stderr)
			return render_template('photo.html',userId=userId, image=e)
	else:
		return render_template('photo.html',userId=userId)
	
@app.route('/register', methods=['POST'])
def register():
	if request.method == "POST":
		try:
			userId = request.form['userId']
			firstname = request.form['firstname']
			lastname = request.form['lastname']
			birthdate = request.form['birthdate']
			birthdate = birthdate.split('-')[2]+"/"+birthdate.split('-')[1]+"/"+birthdate.split('-')[0]
			gender = request.form['gender']
			query = "INSERT INTO User(user_id,first_name,last_name,gender,birth_date) VALUES (?,?,?,?,?)"
			args = (userId,firstname,lastname,gender,birthdate) 
			dbOP.insert(query,args)

		except Exception as e:
			return redirect(url_for('index',error=e))

		return redirect(url_for('dashboard',userId=userId))
	else:
		return redirect(url_for('index'))

@app.route('/delete', methods=['GET','POST'])
def delete():
	if request.method == "GET":
		try:
			userId = request.args.get('userId')
			result_name = request.args.get('result_name')
			command = "rmdir static\\dataset\\"+userId+"\\"+result_name+"/s /q"
			print(command, file=sys.stderr)
			os.system(command)
			query = 'DELETE FROM Result WHERE result_name = "'+result_name+'"'
			args = ()
			dbOP.delete(query,args)
			return redirect(url_for('dashboard',userId=userId))
		except Exception as e:
			return redirect(url_for('index',error=e))

	
	
	





def store64Image(imgstring64,filename): #store image here
	imgdata = base64.b64decode(imgstring64)
	with open(filename, 'wb') as f:
	    f.write(imgdata)


if __name__ == '__main__':
	
	app.secret_key = 'super secret key'
	app.config['SESSION_TYPE'] = 'filesystem'
	app.run(host='0.0.0.0', debug=True)