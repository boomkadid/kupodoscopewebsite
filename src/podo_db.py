import sqlite3

# create a new database if the database doesn't already exist
with sqlite3.connect('podo.db') as connection:

	# get a cursor object used to execute SQL commands
	c = connection.cursor()

	# create the table
	c.execute('''CREATE TABLE User
						 (user_id INTEGER PRIMARY KEY,
							first_name varchar(20) NOT NULL,
							last_name varchar(20) NOT NULL,
							birth_date varchar(20) NOT NULL)''')
	c.execute('''CREATE TABLE Result
						 (result_id INTEGER PRIMARY KEY,
							image_path varchar(20) NOT NULL,
							result varchar(20) NOT NULL,
							result_date varchar(20) NOT NULL,
							description TEXT,
							user_id INTEGER NOT NULL)''')

	c.execute('INSERT INTO User(first_name,last_name,birth_date) VALUES("admin", "admin", "01/04/1994")')
	c.execute('INSERT INTO User(first_name,last_name,birth_date) VALUES("kasidid", "songsupakit", "01/04/1994")')