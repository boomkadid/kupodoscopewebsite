import _winreg as winreg  
import time,  os, subprocess  
def printtoprinter(filename):
	# Dynamically get path to AcroRD32.exe  
	AcroRD32Path = winreg.QueryValue(winreg.HKEY_CLASSES_ROOT,'Software\\Adobe\\Acrobat\Exe')  
	 
	acroread = AcroRD32Path  
	  
	cmd= '{0} /N /T "{1}" ""'.format(acroread,filename)  
	  
	print(cmd)  
	  
	proc = subprocess.Popen(cmd)    
	time.sleep(5)  

	os.system("TASKKILL /F /IM AcroRD32.exe")

if __name__ == '__main__':
	printtoprinter('hello.pdf')