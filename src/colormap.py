#!/usr/bin/python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import numeric
upper,lower = numeric.getBound()
def process(filepath):
	image = cv2.imread(filepath+'/1.png')
	image2 = cv2.imread(filepath+'/left.png')
	image3 = cv2.imread(filepath+'/right.png')
	temp = temperature(image)
	temp2 = temperature(image2)
	temp3 = temperature(image3)
	blue = Blue(image)
	cv2.imwrite(filepath+'/2.png',temp)
	cv2.imwrite(filepath+'/3.png',blue)
	cv2.imwrite(filepath+'/left_2.png',temp2)
	cv2.imwrite(filepath+'/right_2.png',temp3)
	
def temperature(image):
	hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)	
	mask = cv2.inRange(hsv, lower, upper)
	image = cv2.bitwise_and(image, image, mask=mask)
	im_gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	equ = cv2.equalizeHist(im_gray)
	im_color = cv2.applyColorMap(equ, cv2.COLORMAP_JET)
	image = cv2.bitwise_and(im_color, im_color,image, mask=mask)
	return image

def Blue(image):
	hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)	
	mask = cv2.inRange(hsv, lower, upper)
	image = cv2.bitwise_and(image, image, mask=mask)
	return image

if __name__=="__main__":
	process('..\\static\\dataset\\12321\\N5VX4NCPC50DPYK36SAA6TT827MN2JB2VA5B4NOT')
	# cap = cv2.VideoCapture(0)
	# while 1:
	# 	ret, frame = cap.read()
	# 	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	# 	mask = cv2.inRange(hsv, lower, upper)
	# 	image = temperature(frame)
	# 	cv2.imshow('im',image)
	# 	if cv2.waitKey(1) & 0xFF == ord('q'):
	# 			break

