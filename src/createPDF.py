#!/usr/bin/python
# -*- coding: utf-8 -*-
from reportlab.pdfgen import canvas #pdf obj
from reportlab.lib.pagesizes import letter
from reportlab.lib.pagesizes import landscape, A4, portrait
from reportlab.platypus import Image
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
import os
import csv
import cv2
import time

def generate_certificate(user,result,times):
	ks_logo = 'static\\img\\ks.png'
	img = 'static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\3.png'
	img_temp = 'static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\2.png'
	img_left = 'static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\left_2.png'
	img_right = 'static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\right_2.png'
	box = 'static\\img\\box.png'
	checked_box = 'static\\img\\checked_box.png'
	pdfmetrics.registerFont(TTFont('THSarabun', 'THSarabun.ttf'))
	[rl,rr] = result['result'].split('&')
	rl = float(rl)
	rr = float (rr)
	# c = canvas.Canvas('3.pdf', pagesize=portrait(A4))
	c = canvas.Canvas('static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\3.pdf', pagesize=portrait(A4))
	c.setFont('THSarabun', 15, leading=None)
	c.drawCentredString(300,650, "ผลการตรวจประเมินเท้าครั้งที่ ".decode('utf-8')
		+str(times)+" วันที่ตรวจ ".decode('utf-8')+ str(result['result_date']))
	c.drawCentredString(300,620, "หมายเลขผู้ใช้ ".decode('utf-8')+str(user['user_id']).decode('utf-8')
		+" ชื่อ ".decode('utf-8')+user['first_name']+' '+user['last_name']
		+ " เพศ".decode('utf-8')+user['gender']
		+" เกิดวันที่ ".decode('utf-8')+user['birth_date'])
	c.line(30,570,570,570) #hight 220 width 270
	c.line(30,570,30,130)
	c.line(30,130,570,130)
	c.line(570,130,570,570)
	c.line(300,570,300,130)
	c.line(30,350,570,350)

	c.drawImage(ks_logo,260,700,width=80, height=80)
	c.drawCentredString(165,550, "ภาพถ่ายเท้า".decode('utf-8'))
	c.drawImage(img, 50, 360, width=230, height=180)
	c.drawCentredString(165,330, "ภาพแนวโน้มการกระจายน้ำหนักที่เท้า".decode('utf-8'))
	c.drawImage(img_temp, 50, 140, width=230, height=180)
	#################################
	thick = False
	result_left = "normal"
	try:
		if rl == 9999: result_left = "normal"
		elif rl < 0.20: result_left = "thin"
		elif 0.20 < rl <= 0.25: result_left = "normal"
		elif rl > 0.25: 
			result_left = "thick"
			thick = True
	except:
		result_left = "normal"

	result_right = "normal"
	try:
		if rr == 9999: result_right = "normal"
		elif rr <0.20: result_right = "thin" 
		elif 0.20 < rr <= 0.25: result_right = "normal"
		elif rr > 0.25: 
			result_right = "thick"
			thick = True
	except:
		result_right = "normal"

	print result_right,result_left
	#############################
	c.drawString(310,530, "ผลตรวจ".decode('utf-8'))
	c.drawString(310,510, "ลักษณะเท้า".decode('utf-8'))
	c.drawString(310,490, "ซ้าย: ".decode('utf-8'))
	if result_left == "normal": c.drawImage(checked_box, 335, 490, width=10, height=10)
	else: c.drawImage(box, 335, 490, width=10, height=10)
	c.drawString(350,490, "ปกติ ".decode('utf-8'))
	if result_left == "thin": c.drawImage(checked_box, 375, 490, width=10, height=10)
	else: c.drawImage(box, 375, 490, width=10, height=10)
	c.drawString(390,490, "โก่ง ".decode('utf-8'))
	if result_left == "thick": c.drawImage(checked_box, 415, 490, width=10, height=10)
	else: c.drawImage(box, 415, 490, width=10, height=10)
	c.drawString(430,490, "แบน ".decode('utf-8'))


	c.drawString(310,470, "ขวา: ".decode('utf-8'))
	if result_right == "normal": c.drawImage(checked_box, 335, 470, width=10, height=10)
	else: c.drawImage(box, 335, 470, width=10, height=10)
	c.drawString(350,470, "ปกติ ".decode('utf-8'))
	if result_right == "thin": c.drawImage(checked_box, 375, 470, width=10, height=10)
	else: c.drawImage(box, 375, 470, width=10, height=10)
	c.drawString(390,470, "โก่ง ".decode('utf-8'))
	if result_right == "thick": c.drawImage(checked_box, 415, 470, width=10, height=10)
	else: c.drawImage(box, 415, 470, width=10, height=10)
	c.drawString(430,470, "แบน ".decode('utf-8'))

	c.drawString(310,310, "สรุปผล".decode('utf-8'))
	c.drawImage(box, 310, 290, width=10, height=10)
	c.drawString(330,290, "ควรพบแพทย์/พยาบาลเพื่อขอคำปรึกษาทันที".decode('utf-8'))
	c.drawImage(box, 310, 270, width=10, height=10)
	c.drawString(330,270, "ค่อนข้างปกติตรวจครั้งต่อไปภายใน 6 เดือน".decode('utf-8'))
	c.drawImage(checked_box, 310, 250, width=10, height=10)
	c.drawString(330,250, "ปกติตรวจครั้งต่อไปภายใน 1 ปี".decode('utf-8'))

	# add header
	c.setFillGray(0.7) #choose your font colour
	c.drawRightString(580,820,"*ผลการตรวจนี้เป็นผลตรวจเบื้องต้นเท่านั้น ผู้รับตรวจควรปรึกษาแพทย์/ผู้เชี่ยวชาญ เพื่อขอคำแนะนำเพิ่มเติม".decode('utf-8'))

	c.showPage()

	c.save()

def generate_normal(user,result,times,typeImg):
	ks_logo = 'static\\img\\ks.png'
	c = canvas.Canvas('2.pdf', pagesize=landscape(A4))
	pdfmetrics.registerFont(TTFont('THSarabun', 'THSarabun.ttf'))
	if typeImg == 1:
		img = 'static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\3.png'
		c = canvas.Canvas('static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\1.pdf', pagesize=landscape(A4))
		c.setFont('THSarabun', 30, leading=None)
		c.drawCentredString(670,430, 'ภาพถ่ายเท้า ครั้งที่ '.decode('utf-8')+str(times))
	else:
		img = 'static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\2.png'
		c = canvas.Canvas('static\\dataset\\'+str(user['user_id'])+'\\'+result['result_name']+'\\2.pdf', pagesize=landscape(A4))
		c.setFont('THSarabun', 30, leading=None)
		c.drawCentredString(670,430, 'ภาพการกระจายน้ำหนักเท้า ครั้งที่ '.decode('utf-8')+str(times))
	[rl,rr] = result['result'].split('&')
	rl = float(rl)
	rr = float (rr)
	c.drawImage(ks_logo, 630, 470, width=80, height=80)
	c.drawImage(img, 50, 100, width=450, height=400)
	c.setFont('THSarabun', 25, leading=None)
	c.drawCentredString(670,380, 'ชื่อ '.decode('utf-8')+user['first_name']+' '+user['last_name'])
	c.drawCentredString(670,350, 'เกิดวันที่ '.decode('utf-8')+user['birth_date'])
	c.setFont('THSarabun', 15, leading=None)
	c.drawCentredString(670,320, 'ถ่ายวันที่ '.decode('utf-8')+result['result_date'])

	c.showPage()

	c.save()

if __name__=='__main__':
	
	user = dict(user_id='1',first_name="Kasidid",last_name="Songsupakit",
    	birth_date="01/04/1994",gender="ชาย")
	result =  dict(result_name='M4FKW8L6IKMDYFE8SNL6TJDDO7EHU6ZF668DMOAD',result="1&2",result_date=time.strftime("%d/%m/%Y"),
				description="no description",user_id='1')
	generate_certificate(user,result,2)

	os.system('3.pdf')