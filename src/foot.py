#!/usr/bin/python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math
import os
from matplotlib import pyplot as plt
from operator import itemgetter
import glob
# from topdf import *
# from toprinter import *
global width,height
width = 1366
height = 768
foot_roi_right = None

def getBound():
	upper = np.array([90+10, 255, 255])
	lower = np.array([60, 80-80, 80-30])
	return upper,lower

def getPosition(event,x,y,flags,param):
	global foot_roi_right
	if event == cv2.EVENT_LBUTTONDOWN:
		hsv = cv2.cvtColor(param, cv2.COLOR_BGR2HSV)
		print hsv[y][x],x,y

def findMaxArea(input_image):
	minArea = 0
	hsv = cv2.cvtColor(input_image, cv2.COLOR_BGR2HSV)
	upper,lower = getBound()
	mask = cv2.inRange(hsv, lower, upper)
	d,contours,hierarchy = cv2.findContours(mask, 1, 2)
	for c in contours:
			if cv2.contourArea(c) > minArea:
				minArea = cv2.contourArea(c)
				cnt = c
	return cnt

def getTopPoint(cnt):
	return tuple(cnt[cnt[:,:,1].argmin()][0])

def archleft(left,display=False):
	global width,height
	try:
		new_mask_left = np.zeros(left.shape[:2], dtype="uint8")
		# getcontour here
		c_left = findMaxArea(left)
		print "Left Area:",cv2.contourArea(c_left)
		thres = 4500
		if left.shape[0] > 1000:
			thres = 60000 
		if 1000 < cv2.contourArea(c_left) < thres:
			print "L: lower than criteria"
			return 0
		cv2.drawContours(new_mask_left, [c_left], -1, (255), -1)
		left = cv2.bitwise_and(left, left, mask=new_mask_left)
		#<<<<<<<<<<<<<<<<<<<< FIND ROI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		rect_left = cv2.minAreaRect(c_left)
		padding = 0
		rect_left = ((rect_left[0][0],rect_left[0][1]),(rect_left[1][0]+padding,rect_left[1][1]+padding),rect_left[2])
		box_left = np.int0(cv2.boxPoints(rect_left))
		box_left = sorted(sorted(box_left,key=itemgetter(0)),key=itemgetter(1))
		times = 1
		if box_left[0][0] < box_left[1][0]:
			pts1_left=  np.float32([box_left[0],
					box_left[1],
					box_left[2],
					box_left[3]])
			pts2_left = np.float32([[0,0],
					[rect_left[1][1]*times,0],
					[0,rect_left[1][0]*times],
					[rect_left[1][1]*times,rect_left[1][0]*times]])

		else:
			pts1_left=  np.float32([box_left[1],
					box_left[0],
					box_left[3],
					box_left[2]])
			pts2_left = np.float32([[0,0],
					[rect_left[1][0]*times,0],
					[0,rect_left[1][1]*times],
					[rect_left[1][0]*times,rect_left[1][1]*times]])
		#<<<<<<<<<<<<<<<<<<<< TRANSFORM >>>>>>>>>>>>>>>>>>>>>>>>>>>>
		M_left = cv2.getPerspectiveTransform(pts1_left,pts2_left)
		foot_roi_left = cv2.warpPerspective(left,M_left,(int(pts2_left[1][0]),int(pts2_left[2][1])))

		#<<<<<<<<<<<<<<<<<<<< GET TOP MOST >>>>>>>>>>>>>>>>>>>>>>>>>>
		l1,l2 = 0.50,0.60 #bound of left forefinger
		h1,w1,ch1 = foot_roi_left.shape
		foot_roi_left1 = foot_roi_left[0:int(h1*0.5),int(w1*l1):int(w1*l2)] #get top half
		# new_mask_left1 = np.zeros(foot_roi_left1.shape[:2], dtype="uint8")
		c_left1 = findMaxArea(foot_roi_left1)		
		(lx,ly) = getTopPoint(c_left1)
		(lx,ly) = (lx+int(w1*l1),ly)
		# cv2.drawContours(new_mask_left1, [c_left1], -1, (255), -1)
		# <<<<<<<<<<<<<<<<<< Find Arch Index >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		h,w = foot_roi_left.shape[:2]
		foot_roi_left1d3 = foot_roi_left[ly:ly+abs(ly-h)/3,0:w]
		foot_roi_left2d3 = foot_roi_left[ly+abs(ly-h)/3:ly+abs(ly-h)*2/3,0:w]
		foot_roi_left3d3 = foot_roi_left[ly+abs(ly-h)*2/3:h,0:w]
		pr1 = cv2.contourArea(findMaxArea(foot_roi_left1d3))
		pr2 = cv2.contourArea(findMaxArea(foot_roi_left2d3))
		pr3 = cv2.contourArea(findMaxArea(foot_roi_left3d3))
		if display:	
			print '<<<<<<<<<<<<<<<<<<<<< LEFT >>>>>>>>>>>>>>>>>>>>>>>'
			print "L: ",pr1,pr2,pr3
			print "Arch Index of Left: ","%0.2f" % float(pr2/(pr1+pr2+pr3))
		
			#<<<<<<<<<<<<<<<<<<<< DRAW LINE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			cv2.line(foot_roi_left,(lx,foot_roi_left.shape[:2][0]),(lx,ly),(0,0,255),1)
			cv2.line(foot_roi_left,(w,ly),(0,ly),(0,0,255),1)
			cv2.line(foot_roi_left,(w,ly+abs(ly-h)/3),(0,ly+abs(ly-h)/3),(0,0,255),1)
			cv2.line(foot_roi_left,(w,ly+abs(ly-h)*2/3),(0,ly+abs(ly-h)*2/3),(0,0,255),1)
			#<<<<<<<<<<<<<<<<<<<< DISPLAY IMAGE >>>>>>>>>>>>>>>>>>>>>>>>>>
			cv2.imshow('left1_1',foot_roi_left1d3)
			cv2.namedWindow('left1_1',cv2.WINDOW_NORMAL)
			cv2.moveWindow('left1_1',int(width*0.3),0)
			cv2.imshow('left1_2',foot_roi_left2d3)
			cv2.namedWindow('left1_2',cv2.WINDOW_NORMAL)
			cv2.moveWindow('left1_2',int(width*0.3),int(height*0.15))
			cv2.imshow('left1_3',foot_roi_left3d3)
			cv2.namedWindow('left1_3',cv2.WINDOW_NORMAL)
			cv2.moveWindow('left1_3',int(width*0.3),int(height*0.3))

			cv2.imshow('left',foot_roi_left)
			cv2.namedWindow('left',cv2.WINDOW_NORMAL)
			cv2.moveWindow('left',int(width*0.4),0)
			cv2.setMouseCallback('left',getPosition,foot_roi_left)
			cv2.waitKey(0)
			if cv2.waitKey(30) & 0xFF == ord('q'):
				'break'
			elif  cv2.waitKey(30) & 0xFF == ord('s'):
				cv2.imwrite('podo.jpg',image)

		return "%0.2f" % float(pr2/(pr1+pr2+pr3))
	except:
		print "FUBAR"
		return 9999	

def archright(right,display=False):
	global width,height
	global foot_roi_right
	try:
		new_mask_right = np.zeros(right.shape[:2], dtype="uint8")
		# getcontour here
		c_right = findMaxArea(right)
		print "Right Area:",cv2.contourArea(c_right)
		thres = 4500
		if right.shape[0] > 1000:
			thres = 60000
		if 1000 < cv2.contourArea(c_right) < thres:
			print "R: lower than criteria"
			return 0
		cv2.drawContours(new_mask_right, [c_right], -1, (255), -1)
		right = cv2.bitwise_and(right, right, mask=new_mask_right)
		#<<<<<<<<<<<<<<<<<<<< FIND ROI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		rect_right = cv2.minAreaRect(c_right)
		padding = 0
		rect_right = ((rect_right[0][0],rect_right[0][1]),(rect_right[1][0]+padding,rect_right[1][1]+padding),rect_right[2])
		box_right = np.int0(cv2.boxPoints(rect_right))
		box_right = sorted(sorted(box_right,key=itemgetter(0)),key=itemgetter(1))
		times = 1
		if box_right[0][0] < box_right[1][0]:
			pts1_right=  np.float32([box_right[0],
					box_right[1],
					box_right[2],
					box_right[3]])
			pts2_right = np.float32([[0,0],
					[rect_right[1][1]*times,0],
					[0,rect_right[1][0]*times],
					[rect_right[1][1]*times,rect_right[1][0]*times]])
		else:
			pts1_right=  np.float32([box_right[1],
					box_right[0],
					box_right[3],
					box_right[2]])
			pts2_right = np.float32([[0,0],
					[rect_right[1][0]*times,0],
					[0,rect_right[1][1]*times],
					[rect_right[1][0]*times,rect_right[1][1]*times]])

		#<<<<<<<<<<<<<<<<<<<< TRANSFORM >>>>>>>>>>>>>>>>>>>>>>>>>>>>
		M_right = cv2.getPerspectiveTransform(pts1_right,pts2_right)
		foot_roi_right = cv2.warpPerspective(right,M_right,(int(pts2_right[1][0]),int(pts2_right[2][1])))

		#<<<<<<<<<<<<<<<<<<<< GET TOP MOST >>>>>>>>>>>>>>>>>>>>>>>>>>
		r1,r2 = 0.40,0.50 #bound of right foreginger
		h2,w2,ch2 = foot_roi_right.shape
		foot_roi_right1 = foot_roi_right[0:int(h2*0.5),int(w2*r1):int(w2*r2)]	#get top half
		# new_mask_left1 = np.zeros(foot_roi_left1.shape[:2], dtype="uint8")
		# new_mask_right1 = np.zeros(foot_roi_right1.shape[:2], dtype="uint8")
		c_right1 = findMaxArea(foot_roi_right1)
		(rx,ry) = getTopPoint(c_right1)
		(rx,ry) = (rx+int(w2*r1),ry)
		# cv2.drawContours(new_mask_left1, [c_left1], -1, (255), -1)
		# cv2.drawContours(new_mask_right1, [c_right1], -1, (255), -1)
		# <<<<<<<<<<<<<<<<<< Find Arch Index >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		h,w = foot_roi_right.shape[:2]
		foot_roi_right1d3 = foot_roi_right[ry:ry+abs(ry-h)/3,0:w]
		foot_roi_right2d3 = foot_roi_right[ry+abs(ry-h)/3:ry+abs(ry-h)*2/3,0:w]
		foot_roi_right3d3 = foot_roi_right[ry+abs(ry-h)*2/3:h,0:w]
		pl1 = cv2.contourArea(findMaxArea(foot_roi_right1d3))
		pl2 = cv2.contourArea(findMaxArea(foot_roi_right2d3))
		pl3 = cv2.contourArea(findMaxArea(foot_roi_right3d3))
		if display:
			print '<<<<<<<<<<<<<<<<<<<<< RIGHT >>>>>>>>>>>>>>>>>>>>>>>'
			print "R: ",pl1,pl2,pl3
			print "Arch Index of Right: ","%0.2f" % float(pl2/(pl1+pl2+pl3))
		
			#<<<<<<<<<<<<<<<<<<<< DRAW LINE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			cv2.line(foot_roi_right,(rx,foot_roi_right.shape[:2][0]),(rx,ry),(0,0,255),1)
			cv2.line(foot_roi_right,(w,ry),(0,ry),(0,0,255),1)
			cv2.line(foot_roi_right,(w,ry+abs(ry-h)/3),(0,ry+abs(ry-h)/3),(0,0,255),1)
			cv2.line(foot_roi_right,(w,ry+abs(ry-h)*2/3),(0,ry+abs(ry-h)*2/3),(0,0,255),1)
			#<<<<<<<<<<<<<<<<<<<< DISPLAY IMAGE >>>>>>>>>>>>>>>>>>>>>>>>>>
			
			cv2.imshow('right',foot_roi_right)
			cv2.namedWindow('right',cv2.WINDOW_NORMAL)
			cv2.moveWindow('right',int(width*0.5),0)
			cv2.setMouseCallback('right',getPosition,foot_roi_right)

			cv2.imshow('right1_1',foot_roi_right1d3)
			cv2.namedWindow('right1_1',cv2.WINDOW_NORMAL)
			cv2.moveWindow('right1_1',int(width*0.6),0)
			cv2.imshow('right1_2',foot_roi_right2d3)
			cv2.namedWindow('right1_2',cv2.WINDOW_NORMAL)
			cv2.moveWindow('right1_2',int(width*0.6),int(height*0.15))
			cv2.imshow('right1_3',foot_roi_right3d3)
			cv2.namedWindow('right1_3',cv2.WINDOW_NORMAL)
			cv2.moveWindow('right1_3',int(width*0.6),int(height*0.3))
			cv2.waitKey(0)
			if cv2.waitKey(30) & 0xFF == ord('q'):
				'break'
			elif  cv2.waitKey(30) & 0xFF == ord('s'):
				cv2.imwrite('podo.jpg',image)

		return "%0.2f" % float(pl2/(pl1+pl2+pl3))
	except:
		print "FUBAR"
		return 9999
	
def archindex(frame):
	global width,height
	global foot_roi_right
	h,w,ch = frame.shape
	left = frame[0:h,0:int(w/2)]
	right = frame[0:h,int(w/2):w]
	new_mask_left = np.zeros(left.shape[:2], dtype="uint8")
	new_mask_right = np.zeros(right.shape[:2], dtype="uint8")
	# getcontour here
	c_left = findMaxArea(left)		
	c_right = findMaxArea(right)

	cv2.drawContours(new_mask_left, [c_left], -1, (255), -1)
	cv2.drawContours(new_mask_right, [c_right], -1, (255), -1)
	left = cv2.bitwise_and(left, left, mask=new_mask_left)
	right = cv2.bitwise_and(right, right, mask=new_mask_right)
	#<<<<<<<<<<<<<<<<<<<< FIND ROI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	rect_left = cv2.minAreaRect(c_left)
	rect_right = cv2.minAreaRect(c_right)
	padding = 0
	rect_left = ((rect_left[0][0],rect_left[0][1]),(rect_left[1][0]+padding,rect_left[1][1]+padding),rect_left[2])
	rect_right = ((rect_right[0][0],rect_right[0][1]),(rect_right[1][0]+padding,rect_right[1][1]+padding),rect_right[2])
	box_left = np.int0(cv2.boxPoints(rect_left))
	box_right = np.int0(cv2.boxPoints(rect_right))
	box_left = sorted(sorted(box_left,key=itemgetter(0)),key=itemgetter(1))
	box_right = sorted(sorted(box_right,key=itemgetter(0)),key=itemgetter(1))
	times = 1
	if box_left[0][0] < box_left[1][0]:
		pts1_left=  np.float32([box_left[0],
				box_left[1],
				box_left[2],
				box_left[3]])
		pts2_left = np.float32([[0,0],
				[rect_left[1][1]*times,0],
				[0,rect_left[1][0]*times],
				[rect_left[1][1]*times,rect_left[1][0]*times]])

	else:
		pts1_left=  np.float32([box_left[1],
				box_left[0],
				box_left[3],
				box_left[2]])
		pts2_left = np.float32([[0,0],
				[rect_left[1][0]*times,0],
				[0,rect_left[1][1]*times],
				[rect_left[1][0]*times,rect_left[1][1]*times]])
	# -------------------------------------------------
	if box_right[0][0] < box_right[1][0]:
		pts1_right=  np.float32([box_right[0],
				box_right[1],
				box_right[2],
				box_right[3]])
		pts2_right = np.float32([[0,0],
				[rect_right[1][1]*times,0],
				[0,rect_right[1][0]*times],
				[rect_right[1][1]*times,rect_right[1][0]*times]])
	else:
		pts1_right=  np.float32([box_right[1],
				box_right[0],
				box_right[3],
				box_right[2]])
		pts2_right = np.float32([[0,0],
				[rect_right[1][0]*times,0],
				[0,rect_right[1][1]*times],
				[rect_right[1][0]*times,rect_right[1][1]*times]])

	#<<<<<<<<<<<<<<<<<<<< TRANSFORM >>>>>>>>>>>>>>>>>>>>>>>>>>>>
	M_left = cv2.getPerspectiveTransform(pts1_left,pts2_left)
	foot_roi_left = cv2.warpPerspective(left,M_left,(int(pts2_left[1][0]),int(pts2_left[2][1])))
	M_right = cv2.getPerspectiveTransform(pts1_right,pts2_right)
	foot_roi_right = cv2.warpPerspective(right,M_right,(int(pts2_right[1][0]),int(pts2_right[2][1])))

	#<<<<<<<<<<<<<<<<<<<< GET TOP MOST >>>>>>>>>>>>>>>>>>>>>>>>>>
	l1,l2 = 0.50,0.60 #bound of left forefinger
	r1,r2 = 0.40,0.50 #bound of right foreginger
	h1,w1,ch1 = foot_roi_left.shape
	foot_roi_left1 = foot_roi_left[0:int(h1*0.5),int(w1*l1):int(w1*l2)] #get top half
	h2,w2,ch2 = foot_roi_right.shape
	foot_roi_right1 = foot_roi_right[0:int(h2*0.5),int(w2*r1):int(w2*r2)]	#get top half
	# new_mask_left1 = np.zeros(foot_roi_left1.shape[:2], dtype="uint8")
	# new_mask_right1 = np.zeros(foot_roi_right1.shape[:2], dtype="uint8")
	c_left1 = findMaxArea(foot_roi_left1)		
	c_right1 = findMaxArea(foot_roi_right1)
	(lx,ly) = getTopPoint(c_left1)
	(lx,ly) = (lx+int(w1*l1),ly)
	(rx,ry) = getTopPoint(c_right1)
	(rx,ry) = (rx+int(w2*r1),ry)
	# cv2.drawContours(new_mask_left1, [c_left1], -1, (255), -1)
	# cv2.drawContours(new_mask_right1, [c_right1], -1, (255), -1)

	#<<<<<<<<<<<<<<<<<<< MAP COLOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>f
	# image = cv2.bitwise_and(frame, frame, mask=new_mask)
	# im_gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
	# equ = cv2.equalizeHist(im_gray)
	# im_color = cv2.applyColorMap(equ, cv2.COLORMAP_JET)
	# image = cv2.bitwise_and(im_color, im_color,image, mask=new_mask)

	# <<<<<<<<<<<<<<<<<< Find Arch Index >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	h,w = foot_roi_left.shape[:2]
	foot_roi_left1d3 = foot_roi_left[ly:ly+abs(ly-h)/3,0:w]
	foot_roi_left2d3 = foot_roi_left[ly+abs(ly-h)/3:ly+abs(ly-h)*2/3,0:w]
	foot_roi_left3d3 = foot_roi_left[ly+abs(ly-h)*2/3:h,0:w]
	pr1 = cv2.contourArea(findMaxArea(foot_roi_left1d3))
	pr2 = cv2.contourArea(findMaxArea(foot_roi_left2d3))
	pr3 = cv2.contourArea(findMaxArea(foot_roi_left3d3))

	h,w = foot_roi_right.shape[:2]
	foot_roi_right1d3 = foot_roi_right[ry:ry+abs(ry-h)/3,0:w]
	foot_roi_right2d3 = foot_roi_right[ry+abs(ry-h)/3:ry+abs(ry-h)*2/3,0:w]
	foot_roi_right3d3 = foot_roi_right[ry+abs(ry-h)*2/3:h,0:w]
	pl1 = cv2.contourArea(findMaxArea(foot_roi_right1d3))
	pl2 = cv2.contourArea(findMaxArea(foot_roi_right2d3))
	pl3 = cv2.contourArea(findMaxArea(foot_roi_right3d3))
	print "L: ",pl1,pl2,pl3
	print "R: ",pr1,pr2,pr3
	print "Arch Index:"
	print " L: ","%0.2f" % float(pl2/(pl1+pl2+pl3))
	print " R: ","%0.2f" % float(pr2/(pr1+pr2+pr3))
	print '<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>'

	#<<<<<<<<<<<<<<<<<<<< DRAW LINE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	cv2.line(foot_roi_right,(rx,foot_roi_right.shape[:2][0]),(rx,ry),(0,0,255),1)
	cv2.line(foot_roi_right,(w,ry),(0,ry),(0,0,255),1)
	cv2.line(foot_roi_right,(w,ry+abs(ry-h)/3),(0,ry+abs(ry-h)/3),(0,0,255),1)
	cv2.line(foot_roi_right,(w,ry+abs(ry-h)*2/3),(0,ry+abs(ry-h)*2/3),(0,0,255),1)
	cv2.line(foot_roi_left,(lx,foot_roi_left.shape[:2][0]),(lx,ly),(0,0,255),1)
	cv2.line(foot_roi_left,(w,ly),(0,ly),(0,0,255),1)
	cv2.line(foot_roi_left,(w,ly+abs(ly-h)/3),(0,ly+abs(ly-h)/3),(0,0,255),1)
	cv2.line(foot_roi_left,(w,ly+abs(ly-h)*2/3),(0,ly+abs(ly-h)*2/3),(0,0,255),1)

	#<<<<<<<<<<<<<<<<<<<< DISPLAY IMAGE >>>>>>>>>>>>>>>>>>>>>>>>>>
	cv2.imshow('origin',frame)
	cv2.namedWindow('origin',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('origin',int(width*0.4),int(height*0.5))
	cv2.setMouseCallback('origin',getPosition,frame)

	cv2.imshow('left1_1',foot_roi_left1d3)
	cv2.namedWindow('left1_1',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('left1_1',int(width*0.3),0)
	cv2.imshow('left1_2',foot_roi_left2d3)
	cv2.namedWindow('left1_2',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('left1_2',int(width*0.3),int(height*0.15))
	cv2.imshow('left1_3',foot_roi_left3d3)
	cv2.namedWindow('left1_3',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('left1_3',int(width*0.3),int(height*0.3))

	cv2.imshow('left',foot_roi_left)
	cv2.namedWindow('left',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('left',int(width*0.4),0)
	cv2.setMouseCallback('left',getPosition,foot_roi_left)

	cv2.imshow('right',foot_roi_right)
	cv2.namedWindow('right',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('right',int(width*0.5),0)
	cv2.setMouseCallback('right',getPosition,foot_roi_right)

	cv2.imshow('right1_1',foot_roi_right1d3)
	cv2.namedWindow('right1_1',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('right1_1',int(width*0.6),0)
	cv2.imshow('right1_2',foot_roi_right2d3)
	cv2.namedWindow('right1_2',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('right1_2',int(width*0.6),int(height*0.15))
	cv2.imshow('right1_3',foot_roi_right3d3)
	cv2.namedWindow('right1_3',cv2.WINDOW_AUTOSIZE)
	cv2.moveWindow('right1_3',int(width*0.6),int(height*0.3))
	cv2.waitKey(0)
	if cv2.waitKey(30) & 0xFF == ord('q'):
		'break'
	elif  cv2.waitKey(30) & 0xFF == ord('s'):
		cv2.imwrite('podo.jpg',image)
		
def splitImage(img):
	h,w,ch = img.shape
	left = img[0:h,0:int(w/2)]	
	right = img[0:h,int(w/2):w]
	return left,right

def pasImage(filepath):
	img = cv2.imread(filepath+'/1.png')
	(left,right) = splitImage(img)
	cv2.imwrite(filepath+'/left.png',left)
	cv2.imwrite(filepath+'/right.png',right)
	return (archleft(left), archright(right))

if __name__ == '__main__':
	# img_list = glob.glob("C:\Users\Kasidid/Desktop/black.bmp")
	img_list = glob.glob("../../../dataset2/21.png")
	for mm in img_list:
		img = cv2.imread(mm)
		# fimg=cv2.flip(img,0)
		# archindex(img)
		h,w,ch = img.shape		
		(result_left,result_right) = (archleft(splitImage(img)[0],True),
			archright(splitImage(img)[1],True))

		print result_left,result_right	