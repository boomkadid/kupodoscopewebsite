#!/usr/bin/python
# -*- coding: utf-8 -*-
import sqlite3

def connect_db():
    return sqlite3.connect("podo.db")

def insert(query,args):
	conn = connect_db()
	c = conn.cursor()
	c.execute(query, args)
	conn.commit()
	c.close()
	conn.close()

def delete(query,args):
    conn = connect_db()
    c = conn.cursor()
    c.execute(query, args)
    conn.commit()
    c.close()
    conn.close()

def query_user(query, args=(), one=False):
    cur = connect_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    # return (rv[0] if rv else None) if one else rv
    return (dict(user_id=rv[0][0],first_name=rv[0][1],last_name=rv[0][2],gender=rv[0][3],
    	birth_date=rv[0][4]) if rv else None) if one else [dict(user_id=row[0],first_name=row[1],last_name=row[2],gender=row[3],birth_date=row[4]) for row in rv]

def query_photo(query, args=(), one=False):
    cur = connect_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    # return (rv[0] if rv else None) if one else rv
    return (dict(result_id=rv[0][0],result_name=rv[0][1],result=rv[0][2],result_date=rv[0][3],description=rv[0][4],user_id=rv[0][5]) if rv else None) if one else [dict(result_id=row[0],result_name=row[1],result=row[2],result_date=row[3],description=row[4],user_id=row[5]) for row in rv]
