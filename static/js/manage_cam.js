var video = document.getElementById('vidDisplay'),
            canvas = document.getElementById('canvas'),
            context = canvas.getContext('2d'),
            photo = document.getElementById('photo'), 
            vendorUrl = window.URL || window.webkitURL;
            navigator.getUserMedia = navigator.getUserMedia || 
                                navigator.webKitGetUserMedia || 
                                navigator.mozGetUserMedia || 
                                navigator.msGetUserMedia || 
                                navigator.oGetUserMedia;
            
        if (navigator.getUserMedia){
            navigator.getUserMedia({
                video:
                {optional:[{sourceId :"1398b3faab388c3c40b75c31e11ac6dc3e3a4aab07973cf72887b53f6c8a624d"}]}
                ,
                audio: false
            },
              handleVideo, videoError);
        }
        
        navigator.mediaDevices.enumerateDevices()
          .then(function(devices) {
            devices.forEach(function(device) {
              console.log(device.kind + ": " + device.label +
                " id = " + device.deviceId);
            });
          })
          .catch(function(err) {
            console.log(err.name + ": " + error.message);
          });

        function handleVideo(stream){
            video.src = vendorUrl.createObjectURL(stream);
            video.play();
        }

        function videoError(e){
            alert("there has some problem ");
        }

        function Play() {
           
            if (video.paused){
                if (navigator.getUserMedia){
                navigator.getUserMedia({video: true, audio: false}, handleVideo, videoError);
                }

                 $(':input[type="submit"]').prop('disabled', true);
                 document.getElementById('capture').innerHTML = "CAPTURE";
                context.scale(1, -1);
                context.translate(0, -1044); //*4
                context.drawImage(video, 0, 0, 1200, 1044);
            }
            else{
                video.pause();
                $(':input[type="submit"]').prop('disabled', false);
                document.getElementById('capture').innerHTML = "RESET";
                context.scale(1, -1);
                context.translate(0, -1044); //*4
                context.drawImage(video, 0, 0, 1200, 1044); //*4
                photo.setAttribute('src',canvas.toDataURL('image/png'));
                document.getElementById('imagebase64').value = canvas.toDataURL('image/png');
            }
            
            console.log(canvas.toDataURL('image/png'))
        }

        function Download(){
            if (canvas.toDataURL('png')==null) return 1 ;
                var gh = canvas.toDataURL('png');
                var a  = document.createElement('a');
                a.href = gh;
                a.download = 'image.png';
                a.click()
        }
        
        function Save(){
            document.getElementById('imagebase64').value = canvas.toDataURL('image/png');
                }